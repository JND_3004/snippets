#!/bin/sh
# POSIX

SRVPATH="/opt/minecraft"
MCUSER="minecraft"

# colors
COLOR_RED='\033[0;31m'
COLOR_YELLOW='\033[0;33m'
COLOR_GREEN='\033[0;32m'
COLOR_NC='\033[0m'

update_upgrade() {
    echo " "
    echo -e "${COLOR_GREEN}Update Server ...${COLOR_NC}"
    apt-get -qq -y update
    #apt-get -qq -y upgrade
}

show_logo() {
    # http://patorjk.com/software/taag/#p=display&c=bash&f=Big&t=C%20MC%20SRV%0A%2C......................%2C%0A%20%20%20V%20.%201%20.%200%20.%200
cat << EOF
     _____   __  __  _____    _____ _______      __ 
    / ____| |  \/  |/ ____|  / ____|  __ \ \    / / 
   | |      | \  / | |      | (___ | |__) \ \  / /  
   | |      | |\/| | |       \___ \|  _  / \ \/ /   
   | |____  | |  | | |____   ____) | | \ \  \  /    
    \_____| |_| _|_|\_____| |_____/|_|_ \_\ _\/ _ _ 
   ( |_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| )
   |/ \ \    / /     /_ |      / _ \       / _ \ |/ 
       \ \  / /       | |     | | | |     | | | |   
        \ \/ /        | |     | | | |     | | | |   
         \  /     _   | |  _  | |_| |  _  | |_| |   
          \/     (_)  |_| (_)  \___/  (_)  \___/    
                                                    
                                                    
EOF
}

# Show help
show_help() {
cat << EOF
    ===========================================================================================
    ||                                       EXAMPLES:                                       ||
    ===========================================================================================

    Display this help and exit              bash cmcsrv.sh  -h|-help
    Show version of this script             bash cmcsrv.sh  -v|-version

    Create Minecraft server                 bash cmcsrv.sh  --dl [spigot|craftbukkit]
                                                            [--v|--version "1.18"]
                                                            [--sn|--srvname "Servername"]
                                                            [--m|--motd "Motd text"]
                                                            [--ln|--lvlname "world"]
                                                            [--ci|--connip "mc.example.com"]
                                                            [--oi|--ownerig "DeanLP"]
                                                            [--mp|--max-players 35]
                                                            [--w|--whitelist true]
                                                            [--rpwd|--rcon-passwd "passwd1234"]
                                                            [--rp|--rcon-port 25575]
                                                            [--p|--port 25565]
                                                            [--r|--ram 4096 M|4 G]
                                                            [--ab|--auto-backup 7 d|s|r|sr|ds|dr|dsr]
                                                            --c|--create
    Upgrade Minecraft server                bash cmcsrv.sh [--version "1.18.2"] --upgrade
    Fix Minecraft server                    bash cmcsrv.sh --fix
    Remove Minecraft server                 bash cmcsrv.sh --remove

    Start Minecraft server                  bash cmcsrv.sh --start
    Get Status from Minecraft server        bash cmcsrv.sh --status
    Get Live-Log from Minecraft server      bash cmcsrv.sh --live-log
    Restart Minecraft server                bash cmcsrv.sh --restart
    Stop Minecraft server                   bash cmcsrv.sh --stop

EOF
}

# Show version
show_version() {
cat << EOF
                     Author: Justin Dittmer
                    Website: jdittmer.dev   
                     GitLab: -

EOF
}

function config {
    echo $(cat "$SRVPATH/_cfg.cmcsrv" | grep -o "$1:.*" | cut -d':' -f2)
}

function rcon {
    $SRVPATH/_tools/mcrcon/mcrcon -H $(config "connip") -P $(config "rcon_port") -p $(config "rcon_passwd") "$1" > /dev/null
}

MC_DOWNLOAD="craftbukkit"
MC_VERSION="1.18"
MC_SRVNAME=""
MC_MOTD="Created with CMCSRV by JDittmer.dev"
MC_LVLNAME="world"
MC_CONNIP="127.0.0.1"
MC_OWNERIG=""
MC_MAX_PLAYERS=20
MC_WHITELIST=false
MC_ENRCON=true
MC_RCON_PASSWD="admin12345"
MC_RCON_PORT=25575
MC_PORT=25565
MC_RAM=4096
AUTO_BACKUP=false
AUTO_BACKUP_DAYS=
AUTO_BACKUP_ROUTINE=""

while :; do
    case $1 in
        -h|-help)     # Help
            reset
            show_logo
            show_help
            exit
            ;;
        -v|-version)  # Show last version
            reset
            show_logo
            show_version
            exit
            ;;
        --dl)
            if [ ! $(echo $2 | grep -o -E '^(spigot|craftbukkit)$') ]; then
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: Only Spigot and Craftbukkit are supported at the moment!"
                exit
            else
                MC_DOWNLOAD=$2
                shift
            fi
            ;;
        --v|--version)
            if [ ! $(echo $2 | grep -o -E '^((([0-9]{1,}).([0-9]{1,}))|(([0-9]{1,}).([0-9]{1,}).([0-9]{1,})))$') ]; then
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The version number is invalid!"
                exit
            else               
                if [ $(expr $2 | cut -d'.' -f2) -gt 11 ] && [ $(expr $2 | cut -d'.' -f2) -lt 20 ]; then
                    if wget -q --method=HEAD "https://download.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$2.jar"; then
                        MC_VERSION=$2
                        shift
                    elif wget -q --method=HEAD "https://cdn.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$2.jar"; then
                        MC_VERSION=$2
                        shift
                    else
                        echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The requested URL does not exist."
                        echo "         You tried to install \"$MC_DOWNLOAD $2\". Is that correct?"
                        exit
                    fi
                else
                    echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: Only Minecraft version 1.12 to 1.19 is supported!"
                    exit
                fi
            fi
            ;;
        --sn|--srvname)
            MC_SRVNAME=$2
            shift
            ;;
        --m|--motd)
            MC_MOTD=$2
            shift
            ;;
        --ln|--lvlname)
            MC_LVLNAME=$2
            shift
            ;;
        --ci|--connip)
            MC_CONNIP=$2
            shift
            ;;
        --oi|--ownerig)
            MC_OWNERIG=$2
            shift
            ;;
        --mp|--max-players)
            if [ ! $(expr $2 | grep -o -E '^[0-9]{1,}$') ]; then
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The maximum number of players must only consist of numbers!"
                exit
            else
                MC_MAX_PLAYERS=$2
                shift
            fi
            ;;
        --w|--whitelist)
            if [ ! $(echo $2 | grep -o -E '^(true|false)$') ]; then
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The whitelist can only be true or false!"
                exit
            else
                MC_WHITELIST=$2
                shift
            fi
            ;;
        --rpwd|--rcon-passwd)
            MC_RCON_PASSWD=$2
            
            ;;
        --rp|--rcon-port)
            if [ ! $(expr $2 | grep -o -E '^[0-9]{4,5}$') ]; then
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The RCON port must be 4 or 5 digits and consist of numbers!"
                exit
            else
                MC_RCON_PORT=$2
                shift
            fi
            ;;
        --p|--port)
            if [ ! $(expr $2 | grep -o -E '^[0-9]{4,5}$') ]; then
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The port must be 4 or 5 digits and consist of numbers!"
                exit
            else
                MC_PORT=$2
                shift
            fi
            ;;
        --r|--ram)
            if [ $(echo $3 | grep -o -E '(M|G)') ]; then
                if [ $(echo $3 | grep -o -E '(M)') ]; then
                    if [ ! $(expr $2 | grep -o -E '^[0-9]{4,}$') ]; then
                        echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The amount of RAM must only consist of numbers!"
                        exit
                    else
                        if [ $2 -lt 1024 ]; then
                            echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The RAM must be at least 1024 M!"
                            exit
                        else
                            MC_RAM=$2
                            shift
                        fi
                    fi
                    
                elif [ $(echo $3 | grep -o -E '(G)') ]; then
                    if [ ! $(expr $2 | grep -o -E '^[0-9]{1,}$') ]; then
                        echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The amount of RAM must only consist of numbers!"
                        exit
                    else
                        if [ $2 -lt 1 ]; then
                            echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The RAM must be at least 1 G!"
                            exit
                        else
                            MC_RAM=$(expr $2 \* 1024)
                            shift
                        fi
                    fi
                fi
            else
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: You must specify a size unit (M or G)!"
                exit
            fi
            shift
            ;;
        --ab|--auto-backup)
            AUTO_BACKUP=true

            if [ ! $(expr $2 | grep -o -E '^[0-9]{1,3}$') ]; then
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The days may only consist of numbers and a maximum of three digits!"
                exit
            else
                AUTO_BACKUP_DAYS=$2

                if [ ! $(echo $3 | grep -o -E '^(d|s|r|sr|ds|dr|dsr)$') ]; then
                    echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The backup routine can only be d|s|r|sr|ds|dr|dsr !"
                    exit
                else
                    AUTO_BACKUP_ROUTINE=$3
                    shift
                fi
            fi
            shift
            ;;
        --c|--create)
            reset
            show_logo

            update_upgrade

            echo " "
            echo -e "${COLOR_GREEN}Checking system requirements ...${COLOR_NC}"
            check_requirements=true
            # CHECK JAVA VERSION
            current_java_version=$(java --version | grep "java " | cut -d' ' -f2 | cut -d'.' -f1)
            
            if [ $(echo "$MC_VERSION" | cut -d'.' -f2 | grep -o -E '^(12|13|14|15|16)$') ]; then
                if [ $(expr $current_java_version) -lt 16 ]; then
                    echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Java Version > GOOD!"
                else
                    echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: Java Version > You need Java 15 or lower for this Minecraft version!"
                    if [ $check_requirements == true ]; then
                        check_requirements=false
                    fi
                fi
            elif [ $(echo "$MC_VERSION" | cut -d'.' -f2 | grep -o -E '^(17)$') ]; then
                if [ $(expr $current_java_version) -eq 16 ]; then
                    echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Java Version > GOOD!"
                else
                    echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: Java Version > You need Java 16 for this Minecraft version!"
                    if [ $check_requirements == true ]; then
                        check_requirements=false
                    fi
                fi
            elif [ $(echo "$MC_VERSION" | cut -d'.' -f2 | grep -o -E '^(18|19)$') ]; then
                if [ $(expr $current_java_version) -gt 16 ]; then
                    echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Java Version > GOOD!"
                else
                    echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: Java Version > You need Java 17 or higher for this Minecraft version!"
                    if [ $check_requirements == true ]; then
                        check_requirements=false
                    fi
                fi
            else
                echo -e "   [${COLOR_YELLOW}INFO${COLOR_NC}]: Java Version > This Minecraft version is currently not supported in the script. Please contact the creator for an update!"
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
            fi

            # CHECK AVAILABLE RAM
            free -m | grep -o -E 'Mem: .*|Speicher: .*' > mem.cmcsrv.tmp
            sed -i 's/ \{1,\}/;/g' mem.cmcsrv.tmp
            available_ram=$(cat mem.cmcsrv.tmp | cut -d';' -f7)
            available_ram_for_srv=$(expr $available_ram / 2)

            if [ $(expr $available_ram_for_srv - $MC_RAM) -lt 0 ]; then
                echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: Memory > The Minecraft server requires ${MC_RAM}M RAM. Your server only has ${available_ram}M RAM left."
                echo "           We recommend choosing a maximum of $(($((${available_ram_for_srv} / 1024)) * 1024))M of RAM."
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
            else
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]:       Memory > GOOD!"
            fi
            rm mem.cmcsrv.tmp

            # CHECK FUNCTION WGET
            if command -v wget &> /dev/null; then
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]:         wget > GOOD!"
            else
                echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: wget > The \"wget\" function is not installed on the server! Install wget ..."
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
                apt-get install -y wget
            fi

            # CHECK FUNCTION GIT
            if command -v git &> /dev/null; then
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]:          git > GOOD!"
            else
                echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: git > The \"git\" function is not installed on the server! Install git ..."
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
                apt-get install -y git
            fi

            # CHECK FUNCTION TAR
            if command -v tar &> /dev/null; then
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]:          tar > GOOD!"
            else
                echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: tar > The \"tar\" function is not installed on the server! Install tar ..."
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
                apt-get install -y tar
            fi
            
            if [ $check_requirements == false ]; then
                exit
            fi

            if [ ! -d $SRVPATH ]; then
                echo " "
                echo -e "${COLOR_GREEN}Create Minecraft and tools folder ...${COLOR_NC}"
                mkdir $SRVPATH
                mkdir "$SRVPATH/_tools"
            else
                if [ ! -d "$SRVPATH/_tools" ]; then
                    echo " "
                    echo -e "${COLOR_GREEN}Create tools folder ...${COLOR_NC}"
                    mkdir "$SRVPATH/_tools"
                fi
            fi

            echo " "
            echo -e "${COLOR_GREEN}Create CMCSRV config file ...${COLOR_NC}"
            cat << EOF > $SRVPATH/_cfg.cmcsrv
######################################
###                                ###
###   DO NOT MODIFY THIS FILE!!!   ###
###                                ###
######################################

dl:$MC_DOWNLOAD
version:$MC_VERSION
srvname:$MC_SRVNAME
motd:$MC_MOTD
lvlname:$MC_LVLNAME
connip:$MC_CONNIP
ownerig:$MC_OWNERIG
max_players:$MC_MAX_PLAYERS
whitelist:$MC_WHITELIST
rcon_passwd:$MC_RCON_PASSWD
rcon_port:$MC_RCON_PORT
port:$MC_PORT
ram:$MC_RAM
auto_backup:$AUTO_BACKUP
auto_backup_days:$AUTO_BACKUP_DAYS
auto_backup_routine:$AUTO_BACKUP_ROUTINE
EOF
            
            if [ $AUTO_BACKUP == true ]; then
                echo " "
                echo -e "${COLOR_GREEN}Create backup routine ...${COLOR_NC}"
                if [ ! -d "$SRVPATH/_backups" ]; then
                    mkdir "$SRVPATH/_backups"
                fi

                cat << EOF > $SRVPATH/_backup.sh
######################################
###                                ###
###   DO NOT MODIFY THIS FILE!!!   ###
###                                ###
######################################

#!/bin/bash
SRVPATH="/opt/minecraft"

DAYS=$AUTO_BACKUP_DAYS
ROUTINE=$AUTO_BACKUP_ROUTINE

find \$SRVPATH/_backups/ -type f -mtime +\$DAYS -name '*.gz' -delete

tar --exclude='_tools' --exclude='_backups' -czf \$SRVPATH/_backups/server_planned-\$(date +%F-%H-%M).tar.gz \$SRVPATH
EOF

                if [ $(echo $AUTO_BACKUP_ROUTINE | grep -o -E '(d)') ]; then
                    (crontab -u $(whoami) -l; echo "0 0 * * * $SRVPATH/_backup.sh" ) | crontab -u $(whoami) -
                fi
            fi
            
            if ! getent passwd "$MCUSER" > /dev/null 2>&1; then
                echo " "
                echo -e "${COLOR_GREEN}Create Minecraft user ...${COLOR_NC}"
                adduser $MCUSER --gecos "" --disabled-password --disabled-login > /dev/null
            fi

            echo " "
            echo -e "${COLOR_GREEN}Download and install mcrcon ...${COLOR_NC}"
            cd "${SRVPATH}/_tools" && git clone --quiet https://github.com/Tiiffi/mcrcon.git && cd mcrcon && make > /dev/null && make install > /dev/null && cd /opt
            
            echo " "
            echo -e "${COLOR_GREEN}Download $MC_DOWNLOAD $MC_VERSION ...${COLOR_NC}"
            if wget -q --method=HEAD "https://download.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$MC_VERSION.jar"; then
                wget -q -O "$SRVPATH/server.jar" "https://download.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$MC_VERSION.jar"
            elif wget -q --method=HEAD "https://cdn.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$MC_VERSION.jar"; then
                wget -q -O "$SRVPATH/server.jar" "https://cdn.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$MC_VERSION.jar"
            fi

            echo " "
            echo -e "${COLOR_GREEN}Accept Minecraft EULA ...${COLOR_NC}"
            echo "eula=true" > "$SRVPATH/eula.txt"

            echo " "
            echo -e "${COLOR_GREEN}Grant user and folder rights...${COLOR_NC}"
            chown -cR ${MCUSER}:${MCUSER} ${SRVPATH} > /dev/null

            echo " "
            echo -e "${COLOR_GREEN}Create minecraft.service ...${COLOR_NC}"
            cat << EOF > /lib/systemd/system/minecraft.service
[Unit]
Description=Minecraft Server
After=network.target
[Service]
WorkingDirectory=$SRVPATH/
User=$MCUSER
Group=$MCUSER
Nice=1
KillMode=none
ProtectSystem=full
PrivateDevices=true
NoNewPrivileges=true
ExecStart=/usr/bin/java -Xms${MC_RAM}M -Xmx${MC_RAM}M -jar server.jar nogui
ExecStop=${SRVPATH}/_tools/mcrcon/mcrcon -H ${MC_CONNIP} -P ${MC_RCON_PORT} -p ${MC_RCON_PASSWD} stop
RestartSec=5
Restart=always
[Install]
WantedBy=multi-user.target
EOF

            echo " "
            echo -e "${COLOR_GREEN}Enable and start minecraft.service ...${COLOR_NC}"
            systemctl enable minecraft.service
            systemctl start minecraft.service

            while true; do
                if [ $(systemctl show -p SubState --value minecraft) == "running" ]; then
                    sleep 5
                    break
                fi

                if [ $(systemctl show -p SubState --value minecraft) == "dead" ]; then
                    reset
                    show_logo

                    echo " "
                    echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: An error occurred while creating the server!"
                    echo
                    journalctl -p err -b -u minecraft
                    exit
                fi
            done

            echo " "
            echo -e "${COLOR_GREEN}Configure server.properties ...${COLOR_NC}"
            sed -i "s/level-name=world/level-name=$MC_LVLNAME/gI" "$SRVPATH/server.properties"
            sed -i "s/motd=A Minecraft Server/motd=$MC_MOTD/gI" "$SRVPATH/server.properties"
            sed -i "s/max-players=20/max-players=$MC_MAX_PLAYERS/gI" "$SRVPATH/server.properties"
            sed -i "s/server-ip=/server-ip=$MC_CONNIP/gI" "$SRVPATH/server.properties"
            sed -i "s/white-list=false/white-list=$MC_WHITELIST/gI" "$SRVPATH/server.properties"
            sed -i "s/enforce-whitelist=false/enforce-whitelist=$MC_WHITELIST/gI" "$SRVPATH/server.properties"
            sed -i "s/server-port=25565/server-port=$MC_PORT/gI" "$SRVPATH/server.properties"
            sed -i "s/enable-rcon=false/enable-rcon=$MC_ENRCON/gI" "$SRVPATH/server.properties"
            sed -i "s/rcon.port=25575/rcon.port=$MC_RCON_PORT/gI" "$SRVPATH/server.properties"
            sed -i "s/rcon.password=/rcon.password=$MC_RCON_PASSWD/gI" "$SRVPATH/server.properties"

            echo " "
            echo -e "${COLOR_GREEN}Restarting Minecraft server ...${COLOR_NC}"
            systemctl stop minecraft.service
            pgrep -u $MCUSER | xargs kill -9
            sleep 5
            systemctl start minecraft.service

            if [ ! -z "${MC_OWNERIG}" ]; then
                echo " "
                echo -e "${COLOR_GREEN}Waiting for RCON connection. Please wait a moment ...${COLOR_NC}"
                while true; do
                    if [ "$(cat /opt/minecraft/logs/latest.log | grep -o "RCON running")" == "RCON running" ]; then
                        ${SRVPATH}/_tools/mcrcon/mcrcon -H ${MC_CONNIP} -P ${MC_RCON_PORT} -p ${MC_RCON_PASSWD} "op ${MC_OWNERIG}"
                        break
                    else
                        sleep 1
                    fi
                done
            else
                while true; do
                    if [ "$(cat /opt/minecraft/logs/latest.log | grep -o "Done")" == "Done" ]; then
                        break
                    else
                        sleep 1
                    fi
                done
            fi

            while true; do
                if [ $(systemctl show -p SubState --value minecraft) == "running" ]; then
                    break
                fi

                if [ $(systemctl show -p SubState --value minecraft) == "dead" ]; then
                    reset
                    show_logo

                    echo " "
                    echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: An error occurred while rebooting the server!"
                    echo
                    journalctl -p err -b -u minecraft
                    exit
                fi
            done

            reset
            show_logo

            echo
            echo -e "Your Minecraft server is now reachable on ${COLOR_YELLOW}${MC_CONNIP}${COLOR_NC}"
            echo
            ;;
        --remove)
            reset
            show_logo

            update_upgrade

            echo " "
            echo -e "${COLOR_GREEN}Stop and disable minecraft.service ...${COLOR_NC}"
            systemctl stop minecraft.service
            systemctl disable minecraft.service

            echo " "
            echo -e "${COLOR_GREEN}Remove backup routine ...${COLOR_NC}"
            # -> remove cronjob

            echo " "
            echo -e "${COLOR_GREEN}Remove minecraft user and files ...${COLOR_NC}"
            rm /lib/systemd/system/minecraft.service
            pgrep -u $MCUSER | xargs kill -9
            deluser $MCUSER > /dev/null
            cd "${SRVPATH}/_tools/mcrcon" && make uninstall > /dev/null && cd /opt
            rm -r $SRVPATH

            update_upgrade

            reset
            show_logo
            echo
            echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Your Minecraft server including user and service has been deleted!"
            echo
            ;;
        --start)
            reset
            show_logo

            echo " "
            echo -e "${COLOR_GREEN}Compare and update the CMCSRV config and server.properties ...${COLOR_NC}"
            sed -i "s/motd:$(config 'motd')/motd:$(cat $SRVPATH/server.properties | grep -o 'motd=.*' | cut -d'=' -f2)/gI" "$SRVPATH/_cfg.cmcsrv"
            sed -i "s/connip:$(config 'connip')/connip:$(cat $SRVPATH/server.properties | grep -o 'server-ip=.*' | cut -d'=' -f2)/gI" "$SRVPATH/_cfg.cmcsrv"
            sed -i "s/max_players:$(config 'max_players')/max_players:$(cat $SRVPATH/server.properties | grep -o 'max-players=.*' | cut -d'=' -f2)/gI" "$SRVPATH/_cfg.cmcsrv"
            sed -i "s/whitelist:$(config 'whitelist')/whitelist:$(cat $SRVPATH/server.properties | grep -o 'white-list=.*' | cut -d'=' -f2)/gI" "$SRVPATH/_cfg.cmcsrv"
            sed -i "s/port:$(config 'port')/port:$(cat $SRVPATH/server.properties | grep -o 'server-port=.*' | cut -d'=' -f2)/gI" "$SRVPATH/_cfg.cmcsrv"
            if [ "$(cat "$SRVPATH/server.properties" | grep -o 'enable-rcon=.*' | cut -d'=' -f2)" == "false" ]; then
                sed -i "s/enable-rcon=false/enable-rcon=true/gI" "$SRVPATH/server.properties"
            fi

            rcon_changes_detected=false
            if [ "$(config 'rcon_passwd')" != "$(cat "$SRVPATH/server.properties" | grep -o 'rcon.password=.*' | cut -d'=' -f2)" ]; then
                rcon_changes_detected=true
            fi
            if [ "$(config 'rcon_port')" != "$(cat "$SRVPATH/server.properties" | grep -o 'rcon.port=.*' | cut -d'=' -f2)" ]; then
                rcon_changes_detected=true
            fi

            MC_CONNIP=$(config 'connip')

            if [ $rcon_changes_detected == true ]; then
                echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Changes to RCON entries detected. minecraft.service will be rebuilt!"

                sed -i "s/rcon_port:$(config 'rcon_port')/rcon_port:$(cat $SRVPATH/server.properties | grep -o 'rcon.port=.*' | cut -d'=' -f2)/gI" "$SRVPATH/_cfg.cmcsrv"
                sed -i "s/rcon_passwd:$(config 'rcon_passwd')/rcon_passwd:$(cat $SRVPATH/server.properties | grep -o 'rcon.password=.*' | cut -d'=' -f2)/gI" "$SRVPATH/_cfg.cmcsrv"

                echo " "
                echo -e "${COLOR_GREEN}Stop and disable minecraft.service ...${COLOR_NC}"
                systemctl stop minecraft.service
                systemctl disable minecraft.service
                rm /lib/systemd/system/minecraft.service

                echo " "
                echo -e "${COLOR_GREEN}Create minecraft.service ...${COLOR_NC}"

                MC_RAM=$(config 'ram')
                MC_RCON_PORT=$(cat "$SRVPATH/server.properties" | grep -o 'rcon.port=.*' | cut -d'=' -f2)
                MC_RCON_PASSWD=$(cat "$SRVPATH/server.properties" | grep -o 'rcon.password=.*' | cut -d'=' -f2)

                cat << EOF > /lib/systemd/system/minecraft.service
[Unit]
Description=Minecraft Server
After=network.target
[Service]
WorkingDirectory=$SRVPATH/
User=$MCUSER
Group=$MCUSER
Nice=1
KillMode=none
ProtectSystem=full
PrivateDevices=true
NoNewPrivileges=true
ExecStart=/usr/bin/java -Xms${MC_RAM}M -Xmx${MC_RAM}M -jar server.jar nogui
ExecStop=${SRVPATH}/_tools/mcrcon/mcrcon -H ${MC_CONNIP} -P ${MC_RCON_PORT} -p ${MC_RCON_PASSWD} stop
RestartSec=5
Restart=always
[Install]
WantedBy=multi-user.target
EOF

                echo " "
                echo -e "${COLOR_GREEN}Enable minecraft.service ...${COLOR_NC}"
                systemctl enable minecraft.service
            fi

            echo " "
            echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Minecraft server is starting! Please wait a moment ..."
            systemctl start minecraft.service
            sleep 2
            while true; do
                if [ "$(cat /opt/minecraft/logs/latest.log | grep -o "Done")" == "Done" ]; then
                    reset
                    show_logo

                    echo
                    echo -e "Your Minecraft server is now reachable on ${COLOR_YELLOW}${MC_CONNIP}${COLOR_NC}"
                    echo
                    break
                else
                    sleep 1
                fi
            done
            ;;
        --status)
            reset
            show_logo

            if [ $(systemctl show -p SubState --value minecraft) == "start" ]; then
                echo " "
                echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Minecraft server is starting!"
                echo
                exit
            fi

            if [ $(systemctl show -p SubState --value minecraft) == "running" ]; then
                echo " "
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Your Minecraft server is online!"
                echo
                exit
            fi

            if [ $(systemctl show -p SubState --value minecraft) == "dead" ]; then
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: Your Minecraft server is offline!"
                echo
                exit
            fi
            ;;
        --live-log)
            reset
            show_logo

            journalctl -f -u minecraft
            ;;
        --restart)
            reset
            show_logo

            while true; do
                if [ $(systemctl show -p SubState --value minecraft) == "dead" ]; then
                    echo " "
                    echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Minecraft server is already stopped!"
                    echo
                    break
                fi

                if [ $(systemctl show -p SubState --value minecraft) == "running" ]; then
                    echo " "
                    echo -e "${COLOR_GREEN}Send global message to server ...${COLOR_NC}"
                    rcon 'tellraw @a [{"text":"[SERVER] ", "color":"gold"},{"text":"The server will be restarted in 10 seconds!", "color":"red"}]'
                    sleep 10
                    rcon 'save-all'
                    
                    echo " "
                    echo -e "${COLOR_GREEN}Stopping minecraft.service ...${COLOR_NC}"
                    systemctl stop minecraft.service

                    if [ $(config 'auto_backup') == "true" ]; then
                        if [ $(config 'auto_backup_routine' | grep -o -E '(r)') ]; then
                            echo " "
                            echo -e "${COLOR_GREEN}Create backup ...${COLOR_NC}"
                            sleep 3
                            find $SRVPATH/_backups/ -type f -mtime +$(config 'auto_backup_days') -name '*.gz' -delete
                            tar --exclude='_tools' --exclude='_backups' -czf $SRVPATH/_backups/server_restarted-$(date +%F-%H-%M).tar.gz $SRVPATH
                        fi
                    fi
                    break
                fi
            done

            sleep 2

            while true; do
                if [ $(systemctl show -p SubState --value minecraft) == "dead" ]; then
                    echo " "
                    echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Your Minecraft server has been stopped!"
                    break
                fi
            done

            echo " "
            echo -e "${COLOR_GREEN}Compare and update the CMCSRV config and server.properties ...${COLOR_NC}"
            sed -i "s/motd:$(config 'motd')/motd:$(cat $SRVPATH/server.properties | grep -o 'motd=.*' | cut -d'=' -f2)/gI" "$SRVPATH/_cfg.cmcsrv"
            sed -i "s/connip:$(config 'connip')/connip:$(cat $SRVPATH/server.properties | grep -o 'server-ip=.*' | cut -d'=' -f2)/gI" "$SRVPATH/_cfg.cmcsrv"
            sed -i "s/max_players:$(config 'max_players')/max_players:$(cat $SRVPATH/server.properties | grep -o 'max-players=.*' | cut -d'=' -f2)/gI" "$SRVPATH/_cfg.cmcsrv"
            sed -i "s/whitelist:$(config 'whitelist')/whitelist:$(cat $SRVPATH/server.properties | grep -o 'white-list=.*' | cut -d'=' -f2)/gI" "$SRVPATH/_cfg.cmcsrv"
            sed -i "s/port:$(config 'port')/port:$(cat $SRVPATH/server.properties | grep -o 'server-port=.*' | cut -d'=' -f2)/gI" "$SRVPATH/_cfg.cmcsrv"
            if [ "$(cat "$SRVPATH/server.properties" | grep -o 'enable-rcon=.*' | cut -d'=' -f2)" == "false" ]; then
                sed -i "s/enable-rcon=false/enable-rcon=true/gI" "$SRVPATH/server.properties"
            fi

            rcon_changes_detected=false
            if [ "$(config 'rcon_passwd')" != "$(cat "$SRVPATH/server.properties" | grep -o 'rcon.password=.*' | cut -d'=' -f2)" ]; then
                rcon_changes_detected=true
            fi
            if [ "$(config 'rcon_port')" != "$(cat "$SRVPATH/server.properties" | grep -o 'rcon.port=.*' | cut -d'=' -f2)" ]; then
                rcon_changes_detected=true
            fi

            MC_CONNIP=$(config 'connip')

            if [ $rcon_changes_detected == true ]; then
                echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Changes to RCON entries detected. minecraft.service will be rebuilt!"

                sed -i "s/rcon_port:$(config 'rcon_port')/rcon_port:$(cat $SRVPATH/server.properties | grep -o 'rcon.port=.*' | cut -d'=' -f2)/gI" "$SRVPATH/_cfg.cmcsrv"
                sed -i "s/rcon_passwd:$(config 'rcon_passwd')/rcon_passwd:$(cat $SRVPATH/server.properties | grep -o 'rcon.password=.*' | cut -d'=' -f2)/gI" "$SRVPATH/_cfg.cmcsrv"

                echo " "
                echo -e "${COLOR_GREEN}Stop and disable minecraft.service ...${COLOR_NC}"
                systemctl stop minecraft.service
                systemctl disable minecraft.service
                rm /lib/systemd/system/minecraft.service

                echo " "
                echo -e "${COLOR_GREEN}Create minecraft.service ...${COLOR_NC}"

                MC_RAM=$(config 'ram')
                MC_RCON_PORT=$(cat "$SRVPATH/server.properties" | grep -o 'rcon.port=.*' | cut -d'=' -f2)
                MC_RCON_PASSWD=$(cat "$SRVPATH/server.properties" | grep -o 'rcon.password=.*' | cut -d'=' -f2)

                cat << EOF > /lib/systemd/system/minecraft.service
[Unit]
Description=Minecraft Server
After=network.target
[Service]
WorkingDirectory=$SRVPATH/
User=$MCUSER
Group=$MCUSER
Nice=1
KillMode=none
ProtectSystem=full
PrivateDevices=true
NoNewPrivileges=true
ExecStart=/usr/bin/java -Xms${MC_RAM}M -Xmx${MC_RAM}M -jar server.jar nogui
ExecStop=${SRVPATH}/_tools/mcrcon/mcrcon -H ${MC_CONNIP} -P ${MC_RCON_PORT} -p ${MC_RCON_PASSWD} stop
RestartSec=5
Restart=always
[Install]
WantedBy=multi-user.target
EOF

                echo " "
                echo -e "${COLOR_GREEN}Enable minecraft.service ...${COLOR_NC}"
                systemctl enable minecraft.service
            fi

            echo " "
            echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Minecraft server is starting! Please wait a moment ..."
            systemctl start minecraft.service
            sleep 2
            while true; do
                if [ "$(cat $SRVPATH/logs/latest.log | grep -o "Done")" == "Done" ]; then
                    reset
                    show_logo

                    echo
                    echo -e "Your Minecraft server is now reachable on ${COLOR_YELLOW}${MC_CONNIP}${COLOR_NC}"
                    echo
                    break
                else
                    sleep 1
                fi
            done
            ;;
        --stop)
            reset
            show_logo

            while true; do
                if [ $(systemctl show -p SubState --value minecraft) == "dead" ]; then
                    echo " "
                    echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Minecraft server is already stopped!"
                    echo
                    exit
                fi

                if [ $(systemctl show -p SubState --value minecraft) == "running" ]; then
                    echo " "
                    echo -e "${COLOR_GREEN}Send global message to server ...${COLOR_NC}"
                    rcon 'tellraw @a [{"text":"[SERVER] ", "color":"gold"},{"text":"The server will be stopped in 10 seconds!", "color":"red"}]'
                    sleep 10
                    rcon 'save-all'
                    
                    echo " "
                    echo -e "${COLOR_GREEN}Stopping minecraft.service ...${COLOR_NC}"
                    systemctl stop minecraft.service

                    if [ $(config 'auto_backup') == "true" ]; then
                        if [ $(config 'auto_backup_routine' | grep -o -E '(s)') ]; then
                            echo " "
                            echo -e "${COLOR_GREEN}Create backup ...${COLOR_NC}"
                            sleep 3
                            find $SRVPATH/_backups/ -type f -mtime +$(config 'auto_backup_days') -name '*.gz' -delete
                            tar --exclude='_tools' --exclude='_backups' -czf $SRVPATH/_backups/server_stopped-$(date +%F-%H-%M).tar.gz $SRVPATH > /dev/null
                        fi
                    fi
                    break
                fi
            done

            sleep 2

            while true; do
                if [ $(systemctl show -p SubState --value minecraft) == "dead" ]; then
                    echo " "
                    echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Your Minecraft server has been stopped!"
                    echo
                    exit
                fi
            done
            ;;
        -?*)
            reset
            show_logo
            echo " "
            echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: There was a problem with the \"$1 $2 $3\" command!"
            echo "         Use \"-h\" for help."
            echo " "
            exit
            ;;
        *)
            break
    esac

    shift
done