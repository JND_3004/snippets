#!/bin/sh
# POSIX

die() {
    printf '%s\n' "$1" >&2
    exit 1
}

USERNAME=
PASSWORD=
all_databases=false
zip_files=false

# Usage help
show_help() {
cat << EOF
    -h                          Display this help and exit
    -v                          Show version of this script
    -c                          Check functions
    -ci                         Check and install functions if missed

    --u [USERNAME]              Username from your MySQL database
    --p ["PASSWORD"]            Password from your MySQL database

    --list                      List all your databases
    --zf                        Compress files on the end
    --all                       Make backup from all your databases
    --dbs [db1 db2 db3 ..]      Make backup from specific databases

    EXAMPLES:
    =================================================
    List all your databases
    bash bkp.sh --u USERNAME --p "PASSWORD" --list

    Make backup from all your databases
    bash bkp.sh --u USERNAME --p "PASSWORD" --all

    Make backup from specific databases
    bash bkp.sh --u USERNAME --p "PASSWORD" --dbs db1 db2 db3

    If you want to compress the files, then write --zf after the specified password.
    For example: bash bkp.sh --u USERNAME --p "PASSWORD" --zf --dbs db1 db2 db3
EOF
}

# Usage version
show_version() {
cat << EOF
       BKP Version 1.0.1
    =======================
     Author: Justin Dittmer
    Website: jdittmer.dev
     GitLab: -
EOF
}

# Check functions
check_functions() {
    if ! command -v mysql &> /dev/null
    then
        echo
        echo "mysql could not be found"
        echo
        exit
    fi

    if ! command -v mysqldump &> /dev/null
    then
        echo
        echo "mysqldump could not be found"
        echo
        exit
    fi

    if ! command -v zip &> /dev/null
    then
        echo
        echo "zip could not be found"
        echo
        exit
    fi
}

while :; do
    case $1 in
        -h)         # Help
            show_help
            exit
            ;;
        -v)         # Show last version
            show_version
            exit
            ;;
        -c)         # Check functions
            check_functions
            exit
            ;;
        -ci)        # Check functions and install if missed
            if ! command -v mysql &> /dev/null
            then
                while true; do
                    read -p "mysql could not be found! Should MariaDB server be installed? [y|n]: " yn
                    case $yn in
                        [Yy]*) apt-get update; apt-get install -y mariadb-server; apt-get update; reset; break;;
                        [Nn]*) break;;
                        *) echo "Please answer yes or no.";;
                    esac
                done
            fi

            if ! command -v zip &> /dev/null
            then
                while true; do
                    read -p "zip could not be found! Should zip be installed? [y|n]: " yn
                    case $yn in
                        [Yy]*) apt-get update; apt-get install -y zip; apt-get update; reset; break;;
                        [Nn]*) break;;
                        *) echo "Please answer yes or no.";;
                    esac
                done
            fi
            ;;
        --u)        # username
            if [ "$2" ]; then
                USERNAME=$2
                shift
            else
                die 'ERROR: "--u" requires a non-empty option argument.'
            fi
            ;;
        --p)        # password
            if [ "$2" ]; then
                PASSWORD=$2
                shift
            else
                die 'ERROR: "--p" requires a non-empty option argument.'
            fi
            ;;
        --list)     # List all databases
            check_functions
            mysql -u $USERNAME -p$PASSWORD -e 'show databases;'
            ;;
        --zf)       # Zipping SQL files
            zip_files=true
            ;;
        --all)      # Backup all databases
            all_databases=true

            currdate=$(date '+%m%d%Y-%H%M%S')
            if [ $zip_files == true ]; then
                check_functions
                mysqldump -u $USERNAME -p$PASSWORD --all-databases > "$currdate-all_databases.sql"

                zip "bkp_alldatabases-$currdate" "$currdate-all_databases.sql"
                rm "$currdate-all_databases.sql"
            else
                check_functions
                mysqldump -u $USERNAME -p$PASSWORD --all-databases > "$currdate-all_databases.sql"
            fi

            echo
            echo "Your databases have been successfully backed up."
            echo "You can find them in the following directory: $(pwd)";
            echo
            ;;
        --dbs)     # Backup specific databases
            if [ $all_databases == false ]; then
                check_functions
                if [ $zip_files == true ]; then
                    currdate=$(date '+%m%d%Y-%H%M%S')
                    temp_folder_name="bkp_databases-$currdate"

                    # create directory for files
                    mkdir $temp_folder_name

                    # backup databases
                    args=("$@") 
                    ELEMENTS=${#args[@]} 
                    
                    for (( i=1;i<$ELEMENTS;i++)); do 
                        mysqldump -u $USERNAME -p$PASSWORD ${args[${i}]} > "$temp_folder_name/${args[${i}]}.sql"
                    done

                    # zipping files
                    zip $temp_folder_name $temp_folder_name/*

                    # delete temp folder
                    rm -r $temp_folder_name

                    echo
                    echo "Your databases have been successfully backed up."
                    echo "You can find them in the following directory: $(pwd)";
                    echo
                else
                    # backup databases
                    args=("$@") 
                    ELEMENTS=${#args[@]} 
                    
                    for (( i=1;i<$ELEMENTS;i++)); do 
                        mysqldump -u $USERNAME -p$PASSWORD ${args[${i}]} > "$(date '+%m%d%Y-%H%M%S')-${args[${i}]}.sql"
                    done

                    echo
                    echo "Your databases have been successfully backed up."
                    echo "You can find them in the following directory: $(pwd)";
                    echo
                fi
            else
                die 'ERROR: "--all" cannot be used when specifying specific databases.'
            fi
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            show_help
            exit
            ;;
        *)
            break
    esac

    shift
done
